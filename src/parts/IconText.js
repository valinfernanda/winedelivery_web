import React from "react";

import Button from "elements/Button";

export default function IconText() {
  return (
    <Button className="brand-text-icon" href="" type="link">
      Wine<span className="text-gray-900">Delivery.</span>
    </Button>
  );
}
