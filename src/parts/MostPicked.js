import React from "react";
import Fade from "react-reveal/Fade";

export default function MostPicked(props) {
  return (
    <section className="container" ref={props.refMostPicked}>
      <Fade bottom>
        <h4 className="mb-3">"Beer is made by men, wine by God..."</h4>
      </Fade>
    </section>
  );
}
