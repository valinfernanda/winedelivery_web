import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Fade from "react-reveal/Fade";
import axios from "axios";
import { Image } from "antd";
import Button from "elements/Button";
// import testi from "assets/images/testi.jpg";
import Loader from "react-loader-spinner";
import bookmark3 from "assets/images/bookmark3.png";

export default function Categories(props) {
  const history = useHistory();
  const [posts, setPosts] = useState([]);
  const [popUp, setPopUp] = useState(false);

  useEffect(() => {
    axios
      .get(
        `https://zax5j10412.execute-api.ap-southeast-1.amazonaws.com/dev/api/product/list?page=1`
      )
      .then((res) => {
        console.log(res, "ini datanya yak");
        setPosts(res.data.value.products);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  console.log(posts, "ini coba posts");
  const handleClick = (id) => {
    console.log("id", id);
    history.push(`/properties/${id}`);
  };

  return (
    <>
      <div className="container">
        <Fade bottom>
          <h4 className="mb-3 font-weight-medium"> Our Wine</h4>

          <div className="container-grid">
            {posts.map((post, index) => (
              <div
                onClick={() => {
                  console.log(index, "INI INDEX YEY");
                }}
                className="item column-4 row-1"
                key={post.id}
              >
                <Fade bottom>
                  <div className="card">
                    <span className="font-weight-light">Choice</span>
                  </div>

                  <div className="meta-wrapper">
                    <Button
                      type="link"
                      // href={`/properties/${post.id}`}
                      // onClick={`/properties/${post.id}`}
                      onClick={() => handleClick(post.id)}
                      className="stretched-link d-block text-gray-800"
                    >
                      {/* <img
                          src={bookmark3}
                          style={{ margin: `10px 5px 0 10px` }}
                        /> */}
                      {/* ini saya comment karna setiap saya pasang ini, data mappingannya seringkali ga muncul */}
                      <Image width={100} height={300} src={post.image} />

                      <h4 className="h4">{post.name}</h4>
                    </Button>
                    <span className="text-gray-500">{post.country}</span>
                    <h6 className="text-gray-500">{post.qty} Left</h6>
                    <h4 className="text-gray-500">${post.price}</h4>
                    <Button className="addCart">ADD TO CART</Button>
                  </div>
                  {/* </div> */}
                </Fade>
              </div>
            ))}
          </div>
        </Fade>
      </div>
    </>
  );
}
