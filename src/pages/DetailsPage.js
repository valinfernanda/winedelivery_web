import React, { useState, useEffect } from "react";

import Fade from "react-reveal/Fade";
import { connect } from "react-redux";

import Header from "parts/Header";

import Footer from "parts/Footer";

// import { checkoutBooking } from "store/actions/checkout";
import { fetchPage } from "store/actions/page";

function DetailsPage({ match }) {
  useEffect(() => {
    fetchDetails();
    console.log(match, "ini data fetch");
  }, []);

  const [details, setDetails] = useState({});

  const fetchDetails = async () => {
    const data = await fetch(
      `https://zax5j10412.execute-api.ap-southeast-1.amazonaws.com/dev/api/product/${match.params.id}`
      // `https://zax5j10412.execute-api.ap-southeast-1.amazonaws.com/dev/api/product/263579`
    );
    console.log(data, "ini hasil data sendiri");
    const details = await data.json();
    console.log(details.value, "ini detail value");
    setDetails(details.value);
  };

  // render() {
  //   const { page, match } = this.props;

  //   if (!page[match.params.id]) return null;

  //   const breadcrumb = [
  //     { pageTitle: "Home", pageHref: "" },
  //     { pageTitle: "Wine Details", pageHref: "" },
  //   ];

  return (
    <>
      {/* <Header {...this.props} />
       */}
      <Header isCentered />
      {/* <PageDetailTitle breadcrumb={breadcrumb} data={page[match.params.id]} /> */}

      {/* <PageDetailTitle data={details.name} /> */}
      {/* <FeaturedImage data={page[match.params.id].imageId} /> */}
      <section className="container">
        <div className="row">
          <div className="col-7 pr-5">
            <Fade bottom>
              I have tried to modify the mapping, so that each image that is
              clicked could have a different ID. But in the log, it always
              displays ID 268281 even though the image is different. I know that
              I really need to spend more time learning more about ReactJS.
              Thank you for checking my website.
            </Fade>
          </div>
        </div>
      </section>

      <Footer />
    </>
  );
  // }
}

const mapStateToProps = (state) => ({
  page: state.page,
});

export default connect(mapStateToProps, { fetchPage })(DetailsPage);
